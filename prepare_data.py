from typing import Sequence, Tuple

import numpy as np
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler


def prepare_tenzor(sequence: Sequence) -> Tuple[MinMaxScaler, np.ndarray]:
    array = np.array(sequence)
    array = array.reshape(len(array), 1)
    scaler = MinMaxScaler()
    array = scaler.fit_transform(array)
    return scaler, array


def show_data(x: Sequence, y: Sequence):
    plt.plot(x, y)
    plt.title('Input - (x), Output - (y)')
    plt.xlabel('Input variable (x)')
    plt.ylabel('Output variable (y)')
    plt.show()


train_x = [i for i in range(-50, 51)]
train_y = [el ** 2 for el in train_x]

test_x = [i for i in range(60, 161)]
test_y = [el ** 2 for el in test_x]

if __name__ == '__main__':
    show_data(train_x, train_y)

train_x_scaler, train_x = prepare_tenzor(train_x)
train_y_scaler, train_y = prepare_tenzor(train_y)

test_y_scaler, test_y = prepare_tenzor(test_y)
test_x_scaler, test_x = prepare_tenzor(test_x)



