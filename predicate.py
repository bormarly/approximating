from keras import models
from matplotlib import pyplot as plt

from prepare_data import train_x_scaler, train_x, train_y, train_y_scaler, test_x, test_y, test_x_scaler, test_y_scaler


model: models.Sequential = models.load_model('model.h5')

y_predict = model.predict(train_x)

y_predict = train_y_scaler.inverse_transform(y_predict)
x_plot = train_x_scaler.inverse_transform(train_x)
y_plot = train_y_scaler.inverse_transform(train_y)


test_y_predicate = model.predict(test_x)
test_y_predicate = train_y_scaler.inverse_transform(test_y_predicate)
test_x_plot = test_x_scaler.inverse_transform(test_x)
test_Y_plot = test_y_scaler.inverse_transform(test_y)

plt.plot(x_plot, y_plot, 'b', label='actual')
plt.plot(x_plot, y_predict, 'r', label='predicate')
plt.title('Input - (x), Output - (y)')
plt.xlabel('Input variable (x)')
plt.ylabel('Output variable (y)')
plt.legend()
plt.show()
plt.figure()
plt.plot(test_x_plot, y_plot, 'b', label='actual')
plt.plot(test_x_plot, test_y_predicate, 'b', label='test')
plt.title('Input - (x), Output - (y)')
plt.xlabel('Input variable (x)')
plt.ylabel('Output variable (y)')
plt.legend()
plt.show()
