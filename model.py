from keras import models, layers
from keras.activations import relu
from keras.utils import plot_model
from matplotlib import pyplot as plt

from prepare_data import train_x, train_y


model = models.Sequential()
model.add(layers.Dense(10, activation='relu', kernel_initializer='he_uniform'))
model.add(layers.Dense(10, activation='relu', kernel_initializer='he_uniform'))
model.add(layers.Dense(1))

model.compile(loss='mse', optimizer='adam', metrics=['mae'])

plot_model(model, to_file='approximation-model.png', show_shapes=True)
# model.summary()

history = model.fit(
    train_x,
    train_y,
    epochs=500,
    batch_size=10
)

model.save('model_new.h5')

train_mea = history.history['mae']
loss = history.history['loss']
epochs = range(1, len(train_mea)+1)
plt.plot(epochs, train_mea, 'b', label='training mea')
plt.title('Training and validation mea')
plt.legend()
plt.show()

plt.figure()
plt.plot(epochs, loss, 'b', label='Training loss')
plt.title('Training and validation loss')
plt.legend()
plt.show()
